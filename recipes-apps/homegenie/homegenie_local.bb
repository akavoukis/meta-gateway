DESCRIPTION = "Installs Homegenie application"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://homegenie/LICENCE.TXT;md5=8742ca673f11373aa5b26f966c03ef49"

RDEPENDS_${PN} = "\
  mono \
  mono-dev \
"

SRC_URI = "file://homegenie/ \
           file://homegenie.service \
"

#TODO compile for arm
INSANE_SKIP_${PN} = "arch"

inherit systemd
SYSTEMD_SERVICE_${PN} = " homegenie.service"

S = "${WORKDIR}/${PN}"

do_install() {
	install -d ${D}/opt
	cp -r ${S}/homegenie ${D}/opt

	unitdir="${D}${systemd_unitdir}/system"
	install -d -m 0755 ${unitdir}
        install -m 0644 ${WORKDIR}/homegenie.service ${unitdir}
}

# Do NOT try to compile the contents in the bz2 file.
do_compile() {
}

FILES_${PN} += "/opt"

