FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"

PACKAGECONFIG_CONFARGS = ""

# Use current latest master of swupdate to get a new 
# feature of using token auth on hawkbit
SRCREV = '5da81f445e7c5cf22a56b6139125c2ba3ab16b79'

SRC_URI += " \
     file://swupdate.cfg \
     file://swupdate.service \
     file://swupdate-env \
     file://swupdate-progress.service \
     file://hwrevision \
     file://fix_report_success.patch \
     file://version_not_same.patch \
     "

do_install_append() {
    install -d ${D}${sysconfdir}
    install -m 644 ${WORKDIR}/swupdate.cfg ${D}${sysconfdir}

    install -m 644 ${WORKDIR}/hwrevision ${D}${sysconfdir}

    install -d ${D}${bindir}
    install -m 0744 ${WORKDIR}/swupdate-env ${D}${bindir}

}
