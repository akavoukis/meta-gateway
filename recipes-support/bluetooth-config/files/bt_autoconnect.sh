#! /bin/sh
echo -e quit | bluetoothctl | grep -e ARDUINO
if [ $? -eq 1 ]; then
        echo Pair not found
        timeout 30s /usr/lib/bluez/test/test-discovery
        timeout 30s /usr/lib/bluez/test/simple-agent hci0 30:14:10:17:02:90
else
        echo Pair found
fi
rfcomm -r connect hci0 30:14:10:17:02:90 1

