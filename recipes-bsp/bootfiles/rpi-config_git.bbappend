do_deploy_append() {
    # I2C support
    if [ -n "${ENABLE_I2C}" ] || [ "${PITFT}" = "1" ]; then
        echo "dtparam=i2c_vc=on" >>${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    fi
}
